package com.aswdc_gpscquiz.Activity;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.aswdc_gpscquiz.R;

public class DialogRetestActivity extends AppCompatActivity {

    Button btnRetest;
    Button btnReview;
    String testid;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dialog_retest);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT) ;
        setTitle("Test - "+getIntent().getStringExtra("TstName"));

        btnRetest = (Button)findViewById(R.id.retest_btn_retest);
        btnReview = (Button)findViewById(R.id.retest_btn_review);
        testid=getIntent().getStringExtra("TstId").toString();
        Log.d("TestidDialog",testid);
        btnReview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(DialogRetestActivity.this, TestActivity.class);
                in.putExtra("TstId", testid);
                in.putExtra("From", "Review");
                in.putExtra("TstName",getIntent().getStringExtra("TstName"));
                startActivity(in);
                finish();
            }
        });

        btnRetest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(DialogRetestActivity.this, TestActivity.class);
                in.putExtra("TstId", testid);
                in.putExtra("From", "Retest");
                in.putExtra("TstName",getIntent().getStringExtra("TstName"));
                //in.putExtra("choice", "retest");

                startActivity(in);
                finish();
            }
        });
    }
}
