package com.aswdc_gpscquiz.Activity;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.astuetz.PagerSlidingTabStrip;
import com.aswdc_gpscquiz.Adapter.PracticePagerAdapter;
import com.aswdc_gpscquiz.Bean.BeanPractice;
import com.aswdc_gpscquiz.DBHelper.DB_Practice;
import com.aswdc_gpscquiz.DBHelper.DB_Revision;
import com.aswdc_gpscquiz.Fragments.PracticeFragment;
import com.aswdc_gpscquiz.R;

import java.util.ArrayList;

public class PracticeActivity extends AppCompatActivity {

    static ViewPager mPager;
    private PagerAdapter mPagerAdapter;
    public static ArrayList<BeanPractice> arrayque;
    DB_Practice dbq;
    Button btnNext,btnLast;
    Button btnShowAnswer;
    Button btnPrevious,btnFirst;
    BeanPractice bq;
    TextView tvAns;
    DB_Revision dbmr;
    static FragmentManager fm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_practice);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT) ;
        setTitle("Practice");

        int categoryID = getIntent().getIntExtra("CategoryID",0);

        ActionBar supportActionBar = getSupportActionBar();
        if (supportActionBar != null) {
            supportActionBar.setDisplayHomeAsUpEnabled(true);
        }

        fm = getSupportFragmentManager();
        tvAns = (TextView) findViewById(R.id.practice_fragment_tv_answer);
        btnShowAnswer = (Button) findViewById(R.id.practice_btn_showAnswer);
        btnPrevious = (Button) findViewById(R.id.practice_btn_previous);
        btnNext = (Button) findViewById(R.id.practice_btn_next);
        mPager = (ViewPager) findViewById(R.id.practice_pager);
        btnFirst= (Button) findViewById(R.id.practice_btn_first);
        btnLast= (Button) findViewById(R.id.practice_btn_last);
        bq = new BeanPractice();
        dbq = new DB_Practice(this);
        dbmr = new DB_Revision(this);

        arrayque = dbq.selectById(categoryID);
        mPagerAdapter = new PracticePagerAdapter(getSupportFragmentManager(), arrayque);
        mPager.setAdapter(mPagerAdapter);
        mPagerAdapter.notifyDataSetChanged();

        PagerSlidingTabStrip tabStrip = (PagerSlidingTabStrip) findViewById(R.id.practice_tabs);
        tabStrip.setViewPager(mPager);

        tabStrip.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                tvAns.setText("");
            }

            @Override
            public void onPageSelected(int position) {
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        btnPrevious.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                previous(mPager.getCurrentItem() + 1);
                mPager.setCurrentItem(mPager.getCurrentItem() - 1);
            }
        });

        btnFirst.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                previous(0);
                mPager.setCurrentItem(0);
            }
        });
        btnLast.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                next(arrayque.size()-1);
                mPager.setCurrentItem(arrayque.size()-1);
            }
        });
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                next(mPager.getCurrentItem() + 1);
                mPager.setCurrentItem(mPager.getCurrentItem() + 1);
            }
        });

        btnShowAnswer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tvAns.setVisibility(View.VISIBLE);
                int id = arrayque.get(mPager.getCurrentItem()).getQuestionID();
                bq = dbq.selectQuestionByID(id);
                tvAns.setText(bq.getAnswer());

                PracticeFragment fragment=(PracticeFragment)mPager.getAdapter().instantiateItem(mPager,mPager.getCurrentItem());
                fragment.trueOption();
            }
        });
        mPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                next(position);
            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

    }

    public void next(int position) {
        if (position == 0){
            btnPrevious.setVisibility(View.INVISIBLE);
            btnFirst.setVisibility(View.INVISIBLE);}
        else{
            btnPrevious.setVisibility(View.VISIBLE);
            btnFirst.setVisibility(View.VISIBLE);}
        if (position == arrayque.size() - 1){
            btnNext.setVisibility(View.INVISIBLE);
            btnLast.setVisibility(View.INVISIBLE);}
        else{
            btnNext.setVisibility(View.VISIBLE);
            btnLast.setVisibility(View.VISIBLE);}
    }

    public void previous(int position) {
        if (position > 1){
            btnNext.setVisibility(View.VISIBLE);
            btnLast.setVisibility(View.VISIBLE);}
        if (position - 1 == 1){
            btnPrevious.setVisibility(View.INVISIBLE);
            btnFirst.setVisibility(View.INVISIBLE);}
        else{
            btnPrevious.setVisibility(View.VISIBLE);
            btnFirst.setVisibility(View.VISIBLE);}
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    @Override
    public void onBackPressed() {
        if (mPager.getCurrentItem() == 0) {
            // If the user is currently looking at the first step, allow the system to handle the
            // Back button. This calls finish() on this activity and pops the back stack.
            super.onBackPressed();
        } else {
            // Otherwise, select the previous step.
            finish();
        }
    }
}
