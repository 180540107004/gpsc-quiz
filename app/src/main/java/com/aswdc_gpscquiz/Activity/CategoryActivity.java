package com.aswdc_gpscquiz.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;

import com.aswdc_gpscquiz.Adapter.CategoryListAdapter;
import com.aswdc_gpscquiz.Bean.BeanCategory;
import com.aswdc_gpscquiz.DBHelper.DB_Category;
import com.aswdc_gpscquiz.R;

import java.util.ArrayList;

public class CategoryActivity extends AppCompatActivity {

    RecyclerView categoryRecyclerView;
    CardView categoryCardView;
    DB_Category dbCategory;
    CategoryListAdapter categoryListAdapter;
    ArrayList<BeanCategory> categoryArrayList = new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category);

        categoryRecyclerView = (RecyclerView) findViewById(R.id.categoryRecyclerView);
        categoryRecyclerView.setLayoutManager(new GridLayoutManager(this,1));
        categoryCardView = (CardView) findViewById(R.id.categoryCardView);

        dbCategory = new DB_Category(this);
        categoryArrayList = new ArrayList<>();

        categoryArrayList = dbCategory.selectCategory();

        categoryListAdapter = new CategoryListAdapter(this, categoryArrayList);
        categoryRecyclerView.setAdapter(categoryListAdapter);
    }

}
