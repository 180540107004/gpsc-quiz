package com.aswdc_gpscquiz.Activity;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;

import com.astuetz.PagerSlidingTabStrip;
import com.aswdc_gpscquiz.Adapter.TestPagerAdapter;
import com.aswdc_gpscquiz.Bean.BeanPractice;
import com.aswdc_gpscquiz.Bean.BeanTest;
import com.aswdc_gpscquiz.DBHelper.DB_Practice;
import com.aswdc_gpscquiz.DBHelper.DB_Test;
import com.aswdc_gpscquiz.Fragments.TestFragment;
import com.aswdc_gpscquiz.R;

import java.util.ArrayList;

public class TestActivity extends AppCompatActivity {

    Button btnEndTest;
    Button btnNext;
    Button btnPrevious;
    private ViewPager mPager;
    public static ArrayList<BeanPractice> arrayTest;
    public static int TotalQue;
    DB_Test dbtd;
    BeanTest bt;
    DB_Practice dbp;
    TestPagerAdapter pagerAdapter;
    public static int tstID = 0;
    public static String strFrom = "", strChoice = "", tstDate = "", tstName = "";



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setTitle("Test - "+getIntent().getStringExtra("TstName"));



        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        ActionBar supportActionBar = getSupportActionBar();
        if (supportActionBar != null) {
            supportActionBar.setDisplayHomeAsUpEnabled(true);
        }

        TestFragment.attempt = 0;
        TestFragment.correct = 0;
        tstID = 0;
        TotalQue = getIntent().getIntExtra("que_no", 0);

        btnEndTest = (Button) findViewById(R.id.test_btn_endTest);
        btnPrevious = (Button) findViewById(R.id.test_btn_previous);
        btnNext = (Button) findViewById(R.id.test_btn_next);
        mPager = (ViewPager) findViewById(R.id.test_pager);
        bt = new BeanTest();
        dbtd = new DB_Test(this);
        dbp = new DB_Practice(this);

        strFrom = getIntent().getStringExtra("From");
        strChoice = getIntent().getStringExtra("choice");

        if (strFrom.equalsIgnoreCase("Review")) {
            tstName = getIntent().getStringExtra("TstName");
            tstID = Integer.parseInt(getIntent().getStringExtra("TstId").toString());
            arrayTest = dbp.selectTestWiseQuestion(tstID);
            btnEndTest.setVisibility(View.INVISIBLE);
        } else if (strFrom.equalsIgnoreCase("Retest")) {
            tstName = getIntent().getStringExtra("TstName");
            tstID = Integer.parseInt(getIntent().getStringExtra("TstId"));
            dbp.updateTestStatus(tstID);
            arrayTest = dbp.selectTestWiseQuestion(tstID);
            dbp.updateResult(tstID, 0, 0);
        } else {
            tstName = getIntent().getStringExtra("TstName");
            tstDate = getIntent().getStringExtra("TstDate");
            arrayTest = dbp.selectRandomQue(tstName, tstDate);
            tstID = dbp.getTeseId(tstName, tstDate);
        }
        pagerAdapter = new TestPagerAdapter(getSupportFragmentManager(), arrayTest);
        mPager.setAdapter(pagerAdapter);

        PagerSlidingTabStrip tabStrip = (PagerSlidingTabStrip) findViewById(R.id.test_tabs);
        tabStrip.setViewPager(mPager);

        btnPrevious.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               /* int a = mPager.getCurrentItem();
                if (a > 0)
                    mPager.setCurrentItem(a - 1);*/
                previous(mPager.getCurrentItem() + 1);
                mPager.setCurrentItem(mPager.getCurrentItem() - 1);
            }
        });

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              /*  int a = mPager.getCurrentItem();
                if (a < arrayTest.size())
                    mPager.setCurrentItem(a + 1);*/
                next(mPager.getCurrentItem() + 1);
                mPager.setCurrentItem(mPager.getCurrentItem() + 1);
            }
        });

        btnEndTest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int correct = dbp.getTotalCorrect(tstID);
                int attemp = dbp.getTotalAttempt(tstID);
                dbp.updateResult(tstID, attemp, correct);
                Intent intent = new Intent(TestActivity.this, ResultActivity.class);
                intent.putExtra("total", String.valueOf(arrayTest.size()));
                intent.putExtra("attempt", attemp + "");
                intent.putExtra("correct", correct + "");
                intent.putExtra("incorrect", (attemp - correct) + "");
                startActivity(intent);
                finish();
            }
        });

        mPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                next(position);
            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    public void next(int position) {
        if (position == 0)
            btnPrevious.setVisibility(View.INVISIBLE);
        else
            btnPrevious.setVisibility(View.VISIBLE);
        if (position == arrayTest.size() - 1)
            btnNext.setVisibility(View.INVISIBLE);
        else
            btnNext.setVisibility(View.VISIBLE);
    }

    public void previous(int position) {
        if (position > 1)
            btnNext.setVisibility(View.VISIBLE);
        if (position - 1 == 1)
            btnPrevious.setVisibility(View.INVISIBLE);
        else
            btnPrevious.setVisibility(View.VISIBLE);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        if (mPager.getCurrentItem() == 0) {
            // If the user is currently looking at the first step, allow the system to handle the
            // Back button. This calls finish() on this activity and pops the back stack.
            super.onBackPressed();
        } else {
            // Otherwise, select the previous step.
            finish();
        }
    }
}
