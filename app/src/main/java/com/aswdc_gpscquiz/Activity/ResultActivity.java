package com.aswdc_gpscquiz.Activity;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.aswdc_gpscquiz.R;

public class ResultActivity extends AppCompatActivity {

    TextView tvTotal;
    TextView tvAttempt;
    TextView tvCorrect;
    TextView tvIncorrect;
    Button btnok;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT) ;
        setTitle("Result");

        Bundle extras = getIntent().getExtras();
        String value1=extras.getString("total");
        String value2=extras.getString("attempt");
        String value3=extras.getString("correct");
        String value4=extras.getString("incorrect");

        tvTotal = (TextView)findViewById(R.id.result_tv_total);
        tvAttempt = (TextView)findViewById(R.id.result_tv_Attempt);
        tvCorrect = (TextView)findViewById(R.id.result_tv_Correct);
        tvIncorrect = (TextView)findViewById(R.id.result_tv_Incorrect);
        btnok = (Button)findViewById(R.id.result_btn_ok);

        tvTotal.setText(value1);
        tvAttempt.setText(value2);
        tvCorrect.setText(value3);
        tvIncorrect.setText(value4);

        btnok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*Intent in=new Intent(ResultActivity.this, RecyclerTestDetailActivity.class);
                in.putExtra("From","New");
                startActivity(in);*/
                finish();

            }
        });
    }
}
