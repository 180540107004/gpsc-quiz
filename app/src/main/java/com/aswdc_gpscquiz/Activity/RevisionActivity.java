package com.aswdc_gpscquiz.Activity;

import android.app.Activity;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.ListView;

import com.aswdc_gpscquiz.Adapter.RevisionAdapter;
import com.aswdc_gpscquiz.Bean.BeanPractice;
import com.aswdc_gpscquiz.DBHelper.DB_Practice;
import com.aswdc_gpscquiz.DBHelper.DB_Revision;
import com.aswdc_gpscquiz.R;

import java.util.ArrayList;

public class RevisionActivity extends AppCompatActivity {

    static ListView lstRev;
    static DB_Revision dbmr;
    static Activity act;
    static ArrayList<BeanPractice> arrayQue;
    BeanPractice bq;
    public static RevisionAdapter adp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_revision);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT) ;
        setTitle("Revision");

        ActionBar supportActionBar = getSupportActionBar();
        if (supportActionBar != null) {
            supportActionBar.setDisplayHomeAsUpEnabled(true);
        }

        lstRev = (ListView) findViewById(R.id.markRev_lst);
        dbmr = new DB_Revision(this);
        act = this;
        final DB_Practice dbp = new DB_Practice(this);
        bq = new BeanPractice();
        arrayQue = dbmr.getAllData();
        adp=new RevisionAdapter(this, arrayQue);
        lstRev.setAdapter(adp);


        /*lstRev.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
               adp.notifyDataSetChanged();
//                ImageView imgRev= (ImageView) view.findViewById(R.id.markRev_img_rev);
//
//                String tagName = imgRev.getTag().toString();
//                String str[] = tagName.split("=");
//                tagName = str[0];
//                String strID = str[1];
//
//                if (tagName.equalsIgnoreCase("Light")) {
//                    imgRev.setImageResource(R.mipmap.ic_fill_mark_rev);
//                    imgRev.setTag("Dark=" + strID);
//                    dbp.updateRev(1, bq.getQuestionID());
//                    bq.setIsFavourite(1);
//                } else {
//                    imgRev.setImageResource(R.mipmap.ic_mark_rev);
//                    imgRev.setTag("Light=" + strID);
//                    dbp.updateRev(0, bq.getQuestionID());
//                    bq.setIsFavourite(0);
//                }
            }
        });*/
    }
    public static  void reload(){
        arrayQue = dbmr.getAllData();
        adp=new RevisionAdapter(act, arrayQue);
        lstRev.setAdapter(adp);
        adp.notifyDataSetChanged();
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }
}
