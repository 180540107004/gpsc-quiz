package com.aswdc_gpscquiz.Activity;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.cardview.widget.CardView;

import com.aswdc_gpscquiz.R;
import com.aswdc_gpscquiz.Utility.Constant;

public class MainActivity extends AppCompatActivity {

    CardView btnPractice;
    CardView btnTest;
    CardView btnRevision;
    CardView btnDeveloper;
    FloatingActionButton btnShare;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT) ;

        btnPractice = (CardView) findViewById(R.id.activity_main_btn_practice);
        btnTest = (CardView) findViewById(R.id.activity_main_btn_test);
        btnRevision = (CardView) findViewById(R.id.activity_main_btn_revision);
        btnDeveloper = (CardView) findViewById(R.id.activity_main_btn_developer);
        btnShare = (FloatingActionButton) findViewById(R.id.activity_main_button_share);

        btnPractice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(MainActivity.this, CategoryActivity.class);
                in.putExtra("From", "New");
                startActivity(in);
            }
        });

        btnTest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(MainActivity.this, RecyclerTestDetailActivity.class);
                in.putExtra("From", "New");
                startActivity(in);
            }
        });

        btnRevision.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(MainActivity.this, RevisionActivity.class);
                in.putExtra("From", "New");
                startActivity(in);
            }
        });
        btnDeveloper.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(MainActivity.this, DeveloperActivity.class);
                in.putExtra("From", "New");
                startActivity(in);
            }
        });

        btnShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent share = new Intent();
                share.setAction("android.intent.action.SEND");
                share.setType("text/plain");
                share.putExtra("android.intent.extra.TEXT", Constant.SharedMessage + "");
                startActivity(share);
            }
        });

    }
    boolean doubleBackToExitPressedOnce = false;
    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Tap Back Again to Exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce=false;
            }
        }, 2000);
    }

}
