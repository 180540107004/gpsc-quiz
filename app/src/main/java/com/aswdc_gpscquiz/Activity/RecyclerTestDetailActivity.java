package com.aswdc_gpscquiz.Activity;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.Toast;

import com.aswdc_gpscquiz.Adapter.LstTestAdapter;
import com.aswdc_gpscquiz.Bean.BeanPractice;
import com.aswdc_gpscquiz.Bean.BeanTest;
import com.aswdc_gpscquiz.Bean.BeanTestWise;
import com.aswdc_gpscquiz.DBHelper.DB_Practice;
import com.aswdc_gpscquiz.DBHelper.DB_Test;
import com.aswdc_gpscquiz.R;

import java.util.ArrayList;

public class RecyclerTestDetailActivity extends AppCompatActivity {

    static DB_Test dbTD;
    DB_Practice dbp;
    RecyclerView recyclerView;
    ArrayList<BeanPractice> arrayPractice;
    static ArrayList<BeanTest> arrayTest;
    ArrayList<BeanTestWise> arrayTestWise;
    static ListView lst;
    static Activity act;
    static LstTestAdapter adapter;
    BeanTestWise bt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recycler_test_detail);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setTitle("Test Detail");

        ActionBar supportActionBar = getSupportActionBar();
        if (supportActionBar != null) {
            supportActionBar.setDisplayHomeAsUpEnabled(true);
        }
        act = this;

        dbp = new DB_Practice(this);
        bt = new BeanTestWise();
        dbTD = new DB_Test(this);
        lst = (ListView) findViewById(R.id.display_lst);
        arrayTest = dbTD.TestDetail();
        if (arrayTest.size() <= 0) {
            Toast.makeText(this, "Create New Test", Toast.LENGTH_SHORT).show();
            Intent in = new Intent(RecyclerTestDetailActivity.this, DialogTestActivity.class);
            startActivity(in);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        arrayTest = dbTD.TestDetail();
        adapter = new LstTestAdapter(act, arrayTest);
        lst.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    public static void refresh() {
        arrayTest = dbTD.TestDetail();
        adapter = new LstTestAdapter(act, arrayTest);
        lst.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.menu_test:
                Intent in = new Intent(RecyclerTestDetailActivity.this, DialogTestActivity.class);
                startActivity(in);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
