package com.aswdc_gpscquiz.Activity;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.aswdc_gpscquiz.DBHelper.DB_Practice;
import com.aswdc_gpscquiz.R;

public class DeleteActivity extends AppCompatActivity {
    DB_Practice dbp;
    Button btnDelete;
    Button btnCancel;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle("Delete");
        setContentView(R.layout.activity_delete);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT) ;

        dbp=new DB_Practice(this);
        btnCancel=(Button)findViewById(R.id.delete_btn_cancel);
        btnDelete=(Button)findViewById(R.id.delete_btn_ok);
        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dbp.delete(Integer.parseInt(getIntent().getStringExtra("TstId").toString()));
                finish();
            }
        });
    btnCancel.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            finish();
        }
    });
    }
}
