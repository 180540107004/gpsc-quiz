package com.aswdc_gpscquiz.Activity;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;

import com.aswdc_gpscquiz.Bean.BeanTest;
import com.aswdc_gpscquiz.DBHelper.DB_Practice;
import com.aswdc_gpscquiz.DBHelper.DB_Test;
import com.aswdc_gpscquiz.R;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DialogTestActivity extends AppCompatActivity {

    EditText edQueNo;
    EditText edTestName;
    Button btnOK;
    int que_no = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dialog_test);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setTitle("Test Detail");
        /*setTitleColor(R.color.colorPrimary);*/
        InputMethodManager inputMethodManager = (InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
        final DB_Practice dbp = new DB_Practice(this);
        edQueNo = (EditText) findViewById(R.id.dialog_ed_NoOfQ);
        edTestName = (EditText) findViewById(R.id.dialog_ed_Name);
        btnOK = (Button) findViewById(R.id.dialog_btn_OK);
        final DB_Test dbtd = new DB_Test(this);

        btnOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edTestName.length() > 0) {

                    if (edQueNo.length() == 0) {
                        edQueNo.setError("Enter No. of Question");
                        edQueNo.requestFocus();
                    } else {
                        int no = Integer.parseInt(edQueNo.getText().toString());
                        if (no <= dbp.getTabelRowCount("MST_Question")) {
                            if (Integer.parseInt(edQueNo.getText().toString()) == 0) {
                                edQueNo.setError("Enter Valid Input");
                                edQueNo.requestFocus();

                            } else {
                                que_no = Integer.parseInt(edQueNo.getText().toString());
                                Boolean flag = dbp.CompareTestName(edTestName.getText().toString());
                                if (flag == true) {
                                    BeanTest bt = new BeanTest();
                                    bt.setTestName(edTestName.getText() + "");
                                    bt.setTotalQuestion(Integer.parseInt(edQueNo.getText().toString()));
                                    bt.setTestCreateDate(new SimpleDateFormat("dd-MM-yy").format(new Date()).toString());
                                    bt.setTotalCorrect(0);
                                    bt.setTotalAttempt(0);

                                    Intent intent = new Intent(DialogTestActivity.this, TestActivity.class);
                                    dbtd.InsertTest(bt);
                                    intent.putExtra("que_no", que_no);
                                    intent.putExtra("From", "newtest");
                                    intent.putExtra("TstName", edTestName.getText().toString());
                                    intent.putExtra("TstDate", bt.getTestCreateDate() + "");
                                    startActivity(intent);
                                    finish();
                                } else {
                                    edTestName.setError("Test Name Already Exist");
                                    edTestName.requestFocus();
                                }
                            }
                        } else {
                            edQueNo.setError("Enter Number of Questions Less than" + dbp.getTabelRowCount("MST_Question") + "");
                            edQueNo.requestFocus();
                        }
                    }
                } else {
                    edTestName.setError("Enter Test Name");
                    edTestName.requestFocus();
                }
                InputMethodManager inputMethodManager = (InputMethodManager) getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
//                inputMethodManager.hideSoftInputFromWindow(v.getWindowToken(), 0);
                inputMethodManager.showSoftInputFromInputMethod(v.getWindowToken(), 0);
            }
        });
    }
}
