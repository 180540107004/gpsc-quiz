package com.aswdc_gpscquiz.Adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.aswdc_gpscquiz.Bean.BeanTest;
import com.aswdc_gpscquiz.DBHelper.DB_Practice;
import com.aswdc_gpscquiz.Activity.DialogRetestActivity;
import com.aswdc_gpscquiz.Activity.RecyclerTestDetailActivity;
import com.aswdc_gpscquiz.R;

import java.util.ArrayList;

public class LstTestAdapter extends BaseAdapter {

    ArrayList<BeanTest> arrayTst;
    Activity act;
    DB_Practice dbp;

    public LstTestAdapter(Activity act, ArrayList<BeanTest> arrayTst) {
        this.act = act;
        this.arrayTst = arrayTst;
    }

    @Override
    public int getCount() {
        return arrayTst.size();
    }

    @Override
    public Object getItem(int position) {
        return arrayTst.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    private class ViewHolder {

        TextView listTvId;
        TextView listTvName;
        TextView listTvCorrect;
        TextView listTvInCorrect;
        TextView listTvAttempt;
        TextView listTvDate;
        ImageView imgDlt;
        LinearLayout listllmain;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        LayoutInflater layoutInflater = act.getLayoutInflater();
        ViewHolder viewHolder;
        dbp = new DB_Practice(act);
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.all_test_detail, null);
            viewHolder = new ViewHolder();

            viewHolder.listTvId = (TextView) convertView.findViewById(R.id.testDetail_tv_id);
            viewHolder.listllmain = (LinearLayout) convertView.findViewById(R.id.testlist_ll_main);
            viewHolder.listTvName = (TextView) convertView.findViewById(R.id.testDetail_tv_name);
            viewHolder.listTvDate = (TextView) convertView.findViewById(R.id.testDetail_tv_date);
            viewHolder.listTvAttempt = (TextView) convertView.findViewById(R.id.testDetail_tv_attempt);
            viewHolder.listTvCorrect = (TextView) convertView.findViewById(R.id.testDetail_tv_correct);
            viewHolder.listTvInCorrect = (TextView) convertView.findViewById(R.id.testDetail_tv_Incorrect);
            viewHolder.imgDlt = (ImageView) convertView.findViewById(R.id.testDetail_iv_delete);
            convertView.setTag(viewHolder);
        }
        viewHolder = (ViewHolder) convertView.getTag();
        int incrt = 0, atmpt = 0;
        incrt = arrayTst.get(position).getTotalAttempt() - arrayTst.get(position).getTotalCorrect();
        atmpt = arrayTst.get(position).getTotalQuestion() - arrayTst.get(position).getTotalAttempt();
        viewHolder.listTvId.setText(arrayTst.get(position).getTestid() + "");
        viewHolder.listTvName.setText(arrayTst.get(position).getTestName() + "");
        final String sid = viewHolder.listTvId.getText().toString();
        viewHolder.imgDlt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog myQuittingDialogBox = new AlertDialog.Builder(act)
                        .setTitle("Confirm Delete")
                        .setMessage("Are you sure want to delete ?")
                        .setIcon(R.mipmap.ic_confirm)
                        .setPositiveButton("OK", new DialogInterface.OnClickListener(){
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dbp.delete(Integer.parseInt(sid));
                                RecyclerTestDetailActivity.refresh();
                                dialog.dismiss();
                            }
                        })
                        .setNegativeButton("Cancle", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })
                        .create();
                myQuittingDialogBox.show();
            }
        });


        viewHolder.listTvName.setText(arrayTst.get(position).getTestName() + "(" + arrayTst.get(position).getTotalQuestion() + ")");
        viewHolder.listTvDate.setText(arrayTst.get(position).getTestCreateDate() + "");
        viewHolder.listTvAttempt.setText("Attempt : " + arrayTst.get(position).getTotalAttempt());
        viewHolder.listTvCorrect.setText("Correct : " + arrayTst.get(position).getTotalCorrect());
        viewHolder.listTvInCorrect.setText("Incorrect : " + incrt);
        viewHolder.listllmain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String tvid = ((TextView) v.findViewById(R.id.testDetail_tv_id)).getText().toString();
                Intent in = new Intent(act, DialogRetestActivity.class);
                Log.d("TestIDHolder",""+tvid);
                in.putExtra("TstId",tvid);
                in.putExtra("TstName", arrayTst.get(position).getTestName());
                act.startActivity(in);
            }
        });
        return convertView;
    }
}
