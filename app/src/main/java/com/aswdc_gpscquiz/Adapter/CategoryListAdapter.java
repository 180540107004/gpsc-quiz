package com.aswdc_gpscquiz.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.cardview.widget.CardView;

import com.aswdc_gpscquiz.Activity.PracticeActivity;
import com.aswdc_gpscquiz.Bean.BeanCategory;
import com.aswdc_gpscquiz.R;

import java.util.ArrayList;

public class CategoryListAdapter extends RecyclerView.Adapter<CategoryListAdapter.UserHolder>{

    Context context;
    ArrayList<BeanCategory> categoryList;
    View.OnClickListener onClickListener;

    // data is passed into the constructor
    public CategoryListAdapter(Context context, ArrayList<BeanCategory> categoryList) {
        this.context = context;
        this.categoryList= categoryList;
    }

    // convenience method for getting data at click position
    public BeanCategory getItem(int id) {
        return categoryList.get(id);
    }

    // total number of rows
    @Override
    public int getItemCount() {
        return categoryList.size();
    }

    // binds the data to the TextView in each row
    @Override
    public void onBindViewHolder(UserHolder userHolder, @SuppressLint("RecyclerView") final int pos) {
        userHolder.tvCategory.setText(categoryList.get(pos).getCategoryName());
        userHolder.categoryCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, PracticeActivity.class);
                intent.putExtra("CategoryID",categoryList.get(pos).getCategoryID());
                //Toast.makeText(context,"position"+pos,Toast.LENGTH_SHORT).show();
                context.startActivity(intent);
//                if(itemClickListener !=null){
//                    itemClickListener.onItemClick(pos);
//                }
            }
        });
    }

    // inflates the row layout from .xml when needed

    @Override
    public UserHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        return new UserHolder(LayoutInflater.from(context).inflate(R.layout.row_category_list,null));

    }

    class UserHolder extends RecyclerView.ViewHolder{
        TextView tvCategory;
        ImageView ivCategory;
        CardView categoryCard;
        public UserHolder(View itemView) {
            super(itemView);
            tvCategory = itemView.findViewById(R.id.tv_CategoryName);
            ivCategory = itemView.findViewById(R.id.iv_CategoryList);
            categoryCard = itemView.findViewById(R.id.categoryCardView);
        }
    }


}
