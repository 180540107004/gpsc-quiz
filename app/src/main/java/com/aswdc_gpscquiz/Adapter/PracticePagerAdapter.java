package com.aswdc_gpscquiz.Adapter;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.aswdc_gpscquiz.Bean.BeanPractice;
import com.aswdc_gpscquiz.Fragments.PracticeFragment;

import java.util.ArrayList;

public class PracticePagerAdapter extends FragmentPagerAdapter {

    ArrayList<BeanPractice> arrayque;
    public PracticePagerAdapter(FragmentManager fm, ArrayList<BeanPractice> arrayque) {
        super(fm);
        this.arrayque=arrayque;
    }

    @Override
    public int getCount() {
        return arrayque.size();
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = new PracticeFragment();
        Bundle args = new Bundle();
        args.putInt("page_position", position);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return "" + (position+1);
    }

}