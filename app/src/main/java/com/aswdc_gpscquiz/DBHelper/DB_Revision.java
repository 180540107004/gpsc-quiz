package com.aswdc_gpscquiz.DBHelper;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.aswdc_gpscquiz.Bean.BeanPractice;
import com.aswdc_gpscquiz.Utility.Constant;
import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

import java.util.ArrayList;

public class DB_Revision extends SQLiteAssetHelper {

    public DB_Revision(Context context) {
        super(context, Constant.DBName, null, Constant.DBVersion);
    }

    public ArrayList<BeanPractice> getAllData() {
        ArrayList<BeanPractice> arrayQue = new ArrayList<BeanPractice>();
        SQLiteDatabase dbread = getReadableDatabase();
        String query = "select * from MST_Question where Favourite=1";
        Cursor cur = dbread.rawQuery(query, null);
        if (cur.moveToFirst()) {
            do {
                BeanPractice BeanPractice = new BeanPractice();
                BeanPractice.setQuestionID(cur.getInt(cur.getColumnIndex("QuestionID")));
                BeanPractice.setQuestion(cur.getString(cur.getColumnIndex("Question")));
                BeanPractice.setOptionA(cur.getString(cur.getColumnIndex("Option_A")));
                BeanPractice.setOptionB(cur.getString(cur.getColumnIndex("Option_B")));
                BeanPractice.setOptionC(cur.getString(cur.getColumnIndex("Option_C")));
                BeanPractice.setOptionD(cur.getString(cur.getColumnIndex("Option_D")));
                BeanPractice.setAnswer(cur.getString(cur.getColumnIndex("Answer")));
                BeanPractice.setTrueOption(cur.getString(cur.getColumnIndex("TrueOption")));
                BeanPractice.setIsFavourite(cur.getInt(cur.getColumnIndex("Favourite")));
                arrayQue.add(BeanPractice);
            } while (cur.moveToNext());
        }
        dbread.close();
        return arrayQue;
    }
}
