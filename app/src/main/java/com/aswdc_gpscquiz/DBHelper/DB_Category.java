package com.aswdc_gpscquiz.DBHelper;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.aswdc_gpscquiz.Bean.BeanCategory;
import com.aswdc_gpscquiz.Utility.Constant;
import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

import java.util.ArrayList;

public class DB_Category extends SQLiteAssetHelper {

    public DB_Category(Context context) {
        super(context, Constant.DBName, null, Constant.DBVersion);
    }

    public ArrayList<BeanCategory> selectCategory() {

        SQLiteDatabase db = getReadableDatabase();
        String strQuery = "Select * FROM MST_Category";
        Cursor cursor = db.rawQuery(strQuery, null);
        ArrayList<BeanCategory> arrayCategory = new ArrayList<BeanCategory>();
        if (cursor.moveToFirst()) {
            do {
                BeanCategory bq = new BeanCategory();
                bq.setCategoryID(cursor.getInt(cursor.getColumnIndex("CategoryID")));
                bq.setCategoryName(cursor.getString(cursor.getColumnIndex("CategoryName")));
                bq.setCategoryNameGUJ(cursor.getString(cursor.getColumnIndex("CategoryNameGUJ")));
                arrayCategory.add(bq);
            } while (cursor.moveToNext());
        }
        db.close();
        return arrayCategory;
    }

}
