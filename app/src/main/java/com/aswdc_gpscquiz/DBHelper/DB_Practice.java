package com.aswdc_gpscquiz.DBHelper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.aswdc_gpscquiz.Bean.BeanPractice;
import com.aswdc_gpscquiz.Bean.BeanTest;
import com.aswdc_gpscquiz.Activity.TestActivity;
import com.aswdc_gpscquiz.Utility.Constant;
import com.aswdc_gpscquiz.Utility.Util;
import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

import java.util.ArrayList;

public class DB_Practice extends SQLiteAssetHelper {

    public DB_Practice(Context context) {
        super(context, Constant.DBName, null, null, Constant.DBVersion);
    }

    public ArrayList<BeanPractice> selectAllQuestion() {
        ArrayList<BeanPractice> arrayq = new ArrayList<BeanPractice>();

        SQLiteDatabase db = getReadableDatabase();
        String strQuery = " Select QuestionID,Question,Option_A,Option_B,Option_C,Option_D,Answer,TrueOption,Favourite from MST_Question";
        Cursor cur = db.rawQuery(strQuery, null);
        if (cur.moveToFirst()) {
            do {
                BeanPractice bq = new BeanPractice();
                bq.setQuestionID(cur.getInt(cur.getColumnIndex("QuestionID")));
                bq.setQuestion(cur.getString(cur.getColumnIndex("Question")));
                bq.setOptionA(cur.getString(cur.getColumnIndex("Option_A")));
                bq.setOptionB(cur.getString(cur.getColumnIndex("Option_B")));
                bq.setOptionC(cur.getString(cur.getColumnIndex("Option_C")));
                bq.setOptionD(cur.getString(cur.getColumnIndex("Option_D")));
                bq.setAnswer(cur.getString(cur.getColumnIndex("Answer")));
                bq.setTrueOption(cur.getString(cur.getColumnIndex("TrueOption")));
                bq.setIsFavourite(cur.getInt(cur.getColumnIndex("Favourite")));
                arrayq.add(bq);
            } while (cur.moveToNext());
        }
        db.close();
        return arrayq;
    }

    public ArrayList<BeanPractice> selectById(int categoryID) {

        SQLiteDatabase database = getReadableDatabase();
        String strQuery = " Select qn.CategoryID, Question, Option_A , Option_B , Option_C, Option_D,Answer,TrueOption,Favourite From MST_Question qn Inner Join MST_Category c on qn.CategoryID=c.CategoryID where c.CategoryID="+categoryID;

        Cursor cursor = database.rawQuery(strQuery, null);

        ArrayList<BeanPractice> questionList = new ArrayList<BeanPractice>();

        if (cursor.moveToFirst()) {
            do {
                BeanPractice bq = new BeanPractice();
                bq.setCategoryID(cursor.getInt(cursor.getColumnIndex("CategoryID"+"")));
                bq.setQuestion(cursor.getString(cursor.getColumnIndex("Question")));
                bq.setOptionA(cursor.getString(cursor.getColumnIndex("Option_A")));
                bq.setOptionB(cursor.getString(cursor.getColumnIndex("Option_B")));
                bq.setOptionC(cursor.getString(cursor.getColumnIndex("Option_C")));
                bq.setOptionD(cursor.getString(cursor.getColumnIndex("Option_D")));
                bq.setAnswer(cursor.getString(cursor.getColumnIndex("Answer")));
                bq.setTrueOption(cursor.getString(cursor.getColumnIndex("TrueOption")));
                bq.setIsFavourite(cursor.getInt(cursor.getColumnIndex("Favourite")));
                questionList.add(bq);
            }
            while (cursor.moveToNext());
        }
        database.close();
        return questionList;
    }


    public ArrayList<BeanTest> selectAllTest() {
        ArrayList<BeanTest> arrayq = new ArrayList<BeanTest>();
        SQLiteDatabase db = getReadableDatabase();
        String strQuery = "Select * from MST_Test";
        Cursor cur = db.rawQuery(strQuery, null);
        if (cur.moveToFirst()) {
            do {
                BeanTest bq = new BeanTest();
                bq.setTestid(cur.getInt(cur.getColumnIndex("TestID")));
                bq.setTestName(cur.getString(cur.getColumnIndex("TestName")));
                bq.setTestCreateDate(cur.getString(cur.getColumnIndex("CreateDate")));
                bq.setTotalQuestion(cur.getInt(cur.getColumnIndex("TotalQuestion")));
                bq.setTotalCorrect(cur.getInt(cur.getColumnIndex("TotalCorrect")));
                bq.setTotalAttempt(cur.getInt(cur.getColumnIndex("TotalAttempt")));
                arrayq.add(bq);
            } while (cur.moveToNext());
        }
        db.close();
        return arrayq;
    }

    public int getTeseId(String Name, String Date) {
        int id;
        SQLiteDatabase db1 = getReadableDatabase();
        String query = "Select TestID from MST_Test where TestName='" + Name + "' and CreateDate='" + Date + "'";
        Cursor cur1 = db1.rawQuery(query, null);
        cur1.moveToFirst();
        id = cur1.getInt(cur1.getColumnIndex("TestID"));
        db1.close();
        return id;
    }

    public ArrayList<BeanPractice> selectRandomQue(String Name, String Date) {
        int id;
        ArrayList<BeanPractice> arrayq = new ArrayList<BeanPractice>();
        ArrayList<Integer> rndarray = Util.randomQuestion(getTabelRowCount("MST_Question"), TestActivity.TotalQue);
        id = getTeseId(Name, Date);

        for (int i = 0; i < rndarray.size(); i++) {
            SQLiteDatabase db = getReadableDatabase();
            String strQuery = "Select QuestionID,Question,Option_A,Option_B,Option_C,Option_D,Answer,TrueOption from MST_Question " +
                    "WHERE QuestionID=" + rndarray.get(i) + "";
            Cursor cur = db.rawQuery(strQuery, null);
            cur.moveToFirst();
            BeanPractice bq = new BeanPractice();
            bq.setQuestionID(cur.getInt(cur.getColumnIndex("QuestionID")));
            bq.setQuestion(cur.getString(cur.getColumnIndex("Question")));
            bq.setOptionA(cur.getString(cur.getColumnIndex("Option_A")));
            bq.setOptionB(cur.getString(cur.getColumnIndex("Option_B")));
            bq.setOptionC(cur.getString(cur.getColumnIndex("Option_C")));
            bq.setOptionD(cur.getString(cur.getColumnIndex("Option_D")));
            bq.setAnswer(cur.getString(cur.getColumnIndex("Answer")));
            bq.setTrueOption(cur.getString(cur.getColumnIndex("TrueOption")));
            SQLiteDatabase dbCV = getWritableDatabase();
            ContentValues cv = new ContentValues();
            cv.put("TestID", id);
            cv.put("QuestionID", bq.getQuestionID());
            cv.put("AnswerStatus", bq.getAnsStatus());
            cv.put("TestStatus", 0);
            cv.put("IsCorrect", 0);
            cv.put("IsAttempt", 0);
            arrayq.add(bq);
            dbCV.insert("MST_TestWiseQuestion", null, cv);
            db.close();
            dbCV.close();
        }
        return arrayq;
    }

    public int getTabelRowCount(String tname) {

        SQLiteDatabase db = getReadableDatabase();
        String strQuery = "Select * from " + tname;
        Cursor cur = db.rawQuery(strQuery, null);
        cur.moveToFirst();
        return cur.getCount();
    }

    public BeanPractice selectQuestionByID(int id) {

        BeanPractice bq = new BeanPractice();
        SQLiteDatabase db = getReadableDatabase();
        String strQuery = "select Answer,TrueOption from MST_Question where QuestionID=" + id;
        Cursor cur = db.rawQuery(strQuery, null);

        if (cur.moveToFirst()) {
            bq.setAnswer(cur.getString(cur.getColumnIndex("Answer")));
            bq.setTrueOption(cur.getString(cur.getColumnIndex("TrueOption")));
        }
        db.close();
        return bq;
    }

    public void updateTestWiseQuestion(String ans, int tstId, int qid, int iscrt) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put("AnswerStatus", ans);
        cv.put("IsAttempt", 1);
        cv.put("IsCorrect", iscrt);
        db.update("MST_TestWiseQuestion", cv, "TestID=" + tstId + " AND QuestionID=" + qid, null);
        db.close();
    }

    public void updateTestStatus(int tstId) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put("AnswerStatus", "f");
        cv.put("IsAttempt", 0);
        cv.put("IsCorrect", 0);
        db.update("MST_TestWiseQuestion", cv, "TestID=" + tstId, null);
        db.close();
    }

    public void updateResult(int testId, int atmpt, int correct) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues cv = new ContentValues();
        //cv.put("TotalCorrect", crt);
        cv.put("TotalAttempt", atmpt);
        cv.put("TotalCorrect", correct);
        db.update("MST_Test", cv, "TestID=" + testId, null);
        db.close();
    }


    public void updateRev(int a, int id) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put("Favourite", a);
        db.update("MST_Question", cv, "QuestionID=" + id, null);
        db.close();
    }

    public boolean CompareTestName(String testName) {
        SQLiteDatabase db = getReadableDatabase();
        String strQuery = "select TestName from MST_Test where TestName= '" + testName + "'";
        Cursor cur = db.rawQuery(strQuery, null);

        Boolean flag = true;
        if (cur.getCount() > 0) {
            flag = false;
        }
        db.close();
        return flag;
    }

    ArrayList<Integer> testQuestion(int testId) {
        ArrayList<Integer> Question = new ArrayList<Integer>();
        SQLiteDatabase db = getReadableDatabase();
        String query = "Select QuestionID from MST_TestWiseQuestion where TestID='" + testId + "'";
        Cursor cur = db.rawQuery(query, null);
        if (cur.moveToFirst()) {
            do {
                Question.add(Integer.parseInt(cur.getString(cur.getColumnIndex("QuestionID"))));
            } while (cur.moveToNext());
        }
        return Question;
    }

    public int getTotalCorrect(int id) {
        int sum;
        SQLiteDatabase db = getReadableDatabase();
        String query = "Select sum(IsCorrect) from MST_TestWiseQuestion where TestID=" + id;
        Cursor cur = db.rawQuery(query, null);
        cur.moveToFirst();
        sum = cur.getInt(cur.getColumnIndex("sum(IsCorrect)"));
        db.close();
        return sum;
    }

    public int getTotalAttempt(int id) {
        int sum;
        SQLiteDatabase db = getReadableDatabase();
        String query = "Select sum(IsAttempt) from MST_TestWiseQuestion where TestID=" + id;
        Cursor cur = db.rawQuery(query, null);
        cur.moveToFirst();
        sum = cur.getInt(cur.getColumnIndex("sum(IsAttempt)"));
        db.close();
        return sum;
    }

    public ArrayList<BeanPractice> selectTestWiseQuestion(int testId) {
        ArrayList<BeanPractice> arrayq = new ArrayList<BeanPractice>();
        ArrayList<Integer> rndarray = testQuestion(testId);
        for (int i = 0; i < rndarray.size(); i++) {
            SQLiteDatabase db = getReadableDatabase();

            String strQuery = "Select mtq.IsCorrect,mtq.IsAttempt,mtq.TestStatus,mtq.AnswerStatus,mtq.QuestionID," +
                    "mtq.TestWiseQuestionID,mq.QuestionID,mq.Question,mq.Option_A,mq.Option_B,mq.Option_C,mq.Option_D," +
                    "mq.Answer,mq.TrueOption from MST_TestWiseQuestion mtq inner join MST_Question mq on " +
                    "mtq.QuestionID=mq.QuestionID where mtq.QuestionID=" + rndarray.get(i) + "";

            Cursor cur = db.rawQuery(strQuery, null);
            cur.moveToFirst();
            BeanPractice bq = new BeanPractice();

            bq.setQuestionID(cur.getInt(cur.getColumnIndex("QuestionID")));
            bq.setQuestion(cur.getString(cur.getColumnIndex("Question")));
            bq.setOptionA(cur.getString(cur.getColumnIndex("Option_A")));
            bq.setOptionB(cur.getString(cur.getColumnIndex("Option_B")));
            bq.setOptionC(cur.getString(cur.getColumnIndex("Option_C")));
            bq.setOptionD(cur.getString(cur.getColumnIndex("Option_D")));
            bq.setAnswer(cur.getString(cur.getColumnIndex("Answer")));
            bq.setTrueOption(cur.getString(cur.getColumnIndex("TrueOption")));
            bq.setAnsStatus(cur.getString(cur.getColumnIndex("AnswerStatus")));
            bq.setIsCorrect(cur.getInt(cur.getColumnIndex("IsCorrect")));
            bq.setIsAttempt(cur.getString(cur.getColumnIndex("IsAttempt")));
            bq.setTestStatus(cur.getString(cur.getColumnIndex("TestStatus")));
            bq.setTestwiseQueID(cur.getInt(cur.getColumnIndex("TestWiseQuestionID")));

            arrayq.add(bq);
            db.close();
        }
        return arrayq;
    }

    public void delete(int id) {
        SQLiteDatabase db = getWritableDatabase();
        db.delete("MST_Test", "TestID=" + id, null);
        db.close();
    }
}

