package com.aswdc_gpscquiz.DBHelper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.aswdc_gpscquiz.Bean.BeanTest;
import com.aswdc_gpscquiz.Utility.Constant;
import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

import java.util.ArrayList;

public class DB_Test extends SQLiteAssetHelper {

    public DB_Test(Context context) {
        super(context, Constant.DBName, null, Constant.DBVersion);
    }

    public ArrayList<BeanTest> TestDetail() {
        ArrayList<BeanTest> arrayTestDetail = new ArrayList<BeanTest>();
        SQLiteDatabase dbread = getReadableDatabase();
        String query = "select TestID,TestName,TotalQuestion,TotalCorrect,TotalAttempt,CreateDate,SubmitDate from MST_Test";
        Cursor cur = dbread.rawQuery(query, null);
        int Count = 0;
        cur.moveToLast();
        if (cur.moveToLast()) {
            do {
                BeanTest btd = new BeanTest();
                btd.setTestName(cur.getString(cur.getColumnIndex("TestName")));
                btd.setTestid(cur.getInt(cur.getColumnIndex("TestID")));
                btd.setTestCreateDate(cur.getString(cur.getColumnIndex("CreateDate")));
                btd.setTestSubmitDate(cur.getString(cur.getColumnIndex("SubmitDate")));
                btd.setTotalCorrect(cur.getInt(cur.getColumnIndex("TotalCorrect")));
                btd.setTotalAttempt(cur.getInt(cur.getColumnIndex("TotalAttempt")));
                btd.setTotalQuestion(cur.getInt(cur.getColumnIndex("TotalQuestion")));
                Count++;
                arrayTestDetail.add(btd);
            } while (cur.moveToPrevious());
        }
        Log.d("Count", Count + "");
        dbread.close();
        return arrayTestDetail;
    }

    public void InsertTest(BeanTest bd) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues cv = new ContentValues();

        cv.put("LanguageID", bd.getLangID());
        cv.put("TestName", bd.getTestName());
        cv.put("TotalQuestion", bd.getTotalQuestion());
        cv.put("CreateDate", bd.getTestCreateDate());
        cv.put("SubmitDate", bd.getTestSubmitDate());
        cv.put("TotalCorrect", bd.getTotalCorrect());
        cv.put("TotalAttempt", bd.getTotalAttempt());

        db.insert("MST_Test", null, cv);
        db.close();
    }



}
