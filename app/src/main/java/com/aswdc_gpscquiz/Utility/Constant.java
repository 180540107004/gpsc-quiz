package com.aswdc_gpscquiz.Utility;


public class Constant {

    public static String DBName="GPSC_Quiz.db";
    public static int DBVersion=1;

    public static final String AdminEmailAddress="aswdc@darshan.ac.in";
    public static final String ASWDCEmailAddress="aswdc@darshan.ac.in";
    public static final String SharedMessage="Download GPSC MCQ App";

}
