package com.aswdc_gpscquiz.Utility;

import java.util.ArrayList;
import java.util.Collections;

public class Util {
    public static ArrayList<Integer> randomQuestion(int totalQue, int testQue){
        ArrayList<Integer> totalQuestion = new ArrayList<Integer>() ;
        ArrayList<Integer> testQuestion = new ArrayList<Integer>() ;
        for(int i=1;i<=totalQue;i++)
            totalQuestion.add(i);

        Collections.shuffle(totalQuestion);
        for(int i=0;i<testQue;i++)   {
            testQuestion.add(totalQuestion.get(i));
            System.out.println(totalQuestion.get(i));}

        return testQuestion;
    }
}
