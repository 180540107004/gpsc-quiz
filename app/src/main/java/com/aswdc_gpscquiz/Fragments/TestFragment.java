package com.aswdc_gpscquiz.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.aswdc_gpscquiz.Bean.BeanPractice;
import com.aswdc_gpscquiz.DBHelper.DB_Practice;
import com.aswdc_gpscquiz.Activity.TestActivity;
import com.aswdc_gpscquiz.R;

public class TestFragment extends Fragment {

    TextView TesttvQueId;
    TextView TesttvQue;
    RadioButton TestrbA;
    RadioButton TestrbB;
    RadioButton TestrbC;
    RadioButton TestrbD;
    RadioGroup TestrbGrp;
    public static int attempt = 0;
    public static int correct = 0;
    int count = 0;
    String checked = "";
    DB_Practice db;
    BeanPractice bp;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_test, container, false);
        Bundle args = getArguments();
        db = new DB_Practice(getActivity());
        count = args.getInt("page_position");
        init(view);
        loadDate(count);
        if (getActivity().getIntent().getStringExtra("From").equalsIgnoreCase("newtest")||getActivity().getIntent().getStringExtra("From").equalsIgnoreCase("Retest")) {
            TestrbA.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (TestrbA.isChecked()) {
                        checked = "A";
                        TestrbA.setBackground(getResources().getDrawable(R.drawable.border_blue));
                        TestrbB.setBackground(getResources().getDrawable(R.drawable.boarder));
                        TestrbC.setBackground(getResources().getDrawable(R.drawable.boarder));
                        TestrbD.setBackground(getResources().getDrawable(R.drawable.boarder));
                        Log.d("TestId", TestActivity.tstID+"");
                        Log.d("TestIdQuestiionID", bp.getQuestionID()+"");
                        bp.setAnsStatus("a");
                        if(bp.getTrueOption().equalsIgnoreCase("a")) bp.setIsCorrect(1);
                        else bp.setIsCorrect(0);
                        db.updateTestWiseQuestion(checked, TestActivity.tstID, bp.getQuestionID(), bp.getIsCorrect());
                        Log.e("correct", bp.getIsCorrect() + "");
                    }
                }
            });
            TestrbB.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (TestrbB.isChecked()) {
                        checked = "B";
                        TestrbB.setBackground(getResources().getDrawable(R.drawable.border_blue));
                        TestrbA.setBackground(getResources().getDrawable(R.drawable.boarder));
                        TestrbC.setBackground(getResources().getDrawable(R.drawable.boarder));
                        TestrbD.setBackground(getResources().getDrawable(R.drawable.boarder));
                        if(bp.getTrueOption().equalsIgnoreCase("b")) bp.setIsCorrect(1);
                        else bp.setIsCorrect(0);
                        bp.setAnsStatus("B");
                        db.updateTestWiseQuestion(checked, TestActivity.tstID, bp.getQuestionID(), bp.getIsCorrect());
                        Log.e("correct", bp.getIsCorrect() + "");
                    }
                }
            });
            TestrbC.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (TestrbC.isChecked()) {
                        checked = "C";
                        TestrbC.setBackground(getResources().getDrawable(R.drawable.border_blue));
                        TestrbB.setBackground(getResources().getDrawable(R.drawable.boarder));
                        TestrbA.setBackground(getResources().getDrawable(R.drawable.boarder));
                        TestrbD.setBackground(getResources().getDrawable(R.drawable.boarder));
                        if(bp.getTrueOption().equalsIgnoreCase("c")) bp.setIsCorrect(1);
                        else bp.setIsCorrect(0);
                        bp.setAnsStatus("C");
                        db.updateTestWiseQuestion(checked, TestActivity.tstID, bp.getQuestionID(), bp.getIsCorrect());
                        Log.e("correct", bp.getIsCorrect() + "");
                    }
                }
            });
            TestrbD.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (TestrbD.isChecked()) {
                        checked = "D";
                        TestrbD.setBackground(getResources().getDrawable(R.drawable.border_blue));
                        TestrbB.setBackground(getResources().getDrawable(R.drawable.boarder));
                        TestrbC.setBackground(getResources().getDrawable(R.drawable.boarder));
                        TestrbA.setBackground(getResources().getDrawable(R.drawable.boarder));
                        if(bp.getTrueOption().equalsIgnoreCase("d")) bp.setIsCorrect(1);
                        else bp.setIsCorrect(0);
                        bp.setAnsStatus("D");
                        db.updateTestWiseQuestion(checked, TestActivity.tstID, bp.getQuestionID(), bp.getIsCorrect());
                        Log.e("correct", bp.getIsCorrect() + "");
                    }
                }
            });
        }else if(getActivity().getIntent().getStringExtra("From").equalsIgnoreCase("Review")){
            if (TestActivity.arrayTest.get(count).getAnsStatus() != null) {
                if (TestActivity.arrayTest.get(count).getAnsStatus().equalsIgnoreCase("A")) {
                    TestrbA.setBackground(getResources().getDrawable(R.drawable.border_red));
                } else if (TestActivity.arrayTest.get(count).getAnsStatus().equalsIgnoreCase("B")) {
                    TestrbB.setBackground(getResources().getDrawable(R.drawable.border_red));
                } else if (TestActivity.arrayTest.get(count).getAnsStatus().equalsIgnoreCase("C")) {
                    TestrbC.setBackground(getResources().getDrawable(R.drawable.border_red));
                } else if (TestActivity.arrayTest.get(count).getAnsStatus().equalsIgnoreCase("D"))  {
                    TestrbD.setBackground(getResources().getDrawable(R.drawable.border_red));
                }
            }
            if (TestActivity.arrayTest.get(count).getTrueOption().equalsIgnoreCase("A")) {
                TestrbA.setBackground(getResources().getDrawable(R.drawable.border_green));
            } else if (TestActivity.arrayTest.get(count).getTrueOption().equalsIgnoreCase("B")) {
                TestrbB.setBackground(getResources().getDrawable(R.drawable.border_green));
            } else if (TestActivity.arrayTest.get(count).getTrueOption().equalsIgnoreCase("C")) {
                TestrbC.setBackground(getResources().getDrawable(R.drawable.border_green));
            } else if (TestActivity.arrayTest.get(count).getTrueOption().equalsIgnoreCase("D")) {
                TestrbD.setBackground(getResources().getDrawable(R.drawable.border_green));
            }
        }
        return view;
    }
    void init(View view ){
        TesttvQueId = (TextView) view.findViewById(R.id.test_fragment_tv_id);
        TesttvQue = (TextView) view.findViewById(R.id.test_fragment_tv_que);
        TestrbA = (RadioButton) view.findViewById(R.id.test_fragment_rb_A);
        TestrbB = (RadioButton) view.findViewById(R.id.test_fragment_rb_B);
        TestrbC = (RadioButton) view.findViewById(R.id.test_fragment_rb_C);
        TestrbD = (RadioButton) view.findViewById(R.id.test_fragment_rb_D);
        TestrbGrp = (RadioGroup) view.findViewById(R.id.test_fragment_rbGrp);
    }
    void loadDate(int count){
        bp = TestActivity.arrayTest.get(count);
        TesttvQueId.setText("Q." + (count+1) + " of " + TestActivity.arrayTest.size());
        TesttvQue.setText(bp.getQuestion());
        if(bp.getAnsStatus().equalsIgnoreCase("a")){
            TestrbA.setBackground(getResources().getDrawable(R.drawable.border_blue));
        }
        if(bp.getAnsStatus().equalsIgnoreCase("b")){
            TestrbB.setBackground(getResources().getDrawable(R.drawable.border_blue));
        }
        if(bp.getAnsStatus().equalsIgnoreCase("c")){
            TestrbC.setBackground(getResources().getDrawable(R.drawable.border_blue));
        }
        if(bp.getAnsStatus().equalsIgnoreCase("d")){
            TestrbD.setBackground(getResources().getDrawable(R.drawable.border_blue));
        }
        TestrbA.setText(bp.getOptionA());
        TestrbB.setText(bp.getOptionB());
        TestrbC.setText(bp.getOptionC());
        TestrbD.setText(bp.getOptionD());
    }

}

