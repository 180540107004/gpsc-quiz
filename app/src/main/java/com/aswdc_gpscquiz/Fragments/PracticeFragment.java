package com.aswdc_gpscquiz.Fragments;

import android.annotation.TargetApi;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.aswdc_gpscquiz.Bean.BeanPractice;
import com.aswdc_gpscquiz.DBHelper.DB_Practice;
import com.aswdc_gpscquiz.Activity.PracticeActivity;
import com.aswdc_gpscquiz.R;

import static com.aswdc_gpscquiz.Activity.PracticeActivity.arrayque;


public class PracticeFragment extends Fragment {

    TextView tvQueId;
    TextView tvQue;
    RadioButton rbA;
    RadioButton rbB;
    RadioButton rbC;
    RadioButton rbD;
    RadioGroup rbGrp;
    ImageView imgRev;
    ViewPager mPager;
    DB_Practice dbp;
    BeanPractice bq;
    int count = 0;

    public static String strChecked = "";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_practice, container, false);
        Bundle args = getArguments();
        count = args.getInt("page_position");
        bq = arrayque.get(count);
        final BeanPractice fbq = arrayque.get(count);
        dbp = new DB_Practice(getActivity());

        tvQueId = (TextView) view.findViewById(R.id.practice_fragment_tv_id);
        tvQue = (TextView) view.findViewById(R.id.practice_fragment_tv_que);
        rbA = (RadioButton) view.findViewById(R.id.practice_fragment_rb_A);
        rbB = (RadioButton) view.findViewById(R.id.practice_fragment_rb_B);
        rbC = (RadioButton) view.findViewById(R.id.practice_fragment_rb_C);
        rbD = (RadioButton) view.findViewById(R.id.practice_fragment_rb_D);
        rbGrp = (RadioGroup) view.findViewById(R.id.practice_fragment_rbGrp);
        imgRev = (ImageView) view.findViewById(R.id.practice_fragment_img_rev);
        mPager = (ViewPager) view.findViewById(R.id.practice_pager);

        tvQueId.setText("Q." + (count + 1) + " of " + arrayque.size());
        tvQue.setText(bq.getQuestion());
        rbA.setText(bq.getOptionA());
        rbB.setText(bq.getOptionB());
        rbC.setText(bq.getOptionC());
        rbD.setText(bq.getOptionD());

        if (bq.getAnsStatus().equalsIgnoreCase("a")) {
            if (bq.getTrueOption().equalsIgnoreCase("a")) {
                rbA.setBackground(getResources().getDrawable(R.drawable.border_green));
                rbA.setButtonTintList(ContextCompat.getColorStateList(getActivity(), R.color.green));
            } else {
                rbA.setBackground(getResources().getDrawable(R.drawable.border_red));
                rbA.setButtonTintList(ContextCompat.getColorStateList(getActivity(), R.color.red));
            }
        } else if (bq.getAnsStatus().equalsIgnoreCase("b")) {
            if (bq.getTrueOption().equalsIgnoreCase("b")) {
                rbB.setBackground(getResources().getDrawable(R.drawable.border_green));
                rbB.setButtonTintList(ContextCompat.getColorStateList(getActivity(), R.color.green));
            } else {
                rbB.setBackground(getResources().getDrawable(R.drawable.border_red));
                rbB.setButtonTintList(ContextCompat.getColorStateList(getActivity(), R.color.red));
            }
        } else if (bq.getAnsStatus().equalsIgnoreCase("c")) {
            if (bq.getTrueOption().equalsIgnoreCase("c")) {
                rbC.setBackground(getResources().getDrawable(R.drawable.border_green));
                rbC.setButtonTintList(ContextCompat.getColorStateList(getActivity(), R.color.green));
            } else {
                rbC.setBackground(getResources().getDrawable(R.drawable.border_red));
                rbC.setButtonTintList(ContextCompat.getColorStateList(getActivity(), R.color.red));
            }
        } else if (bq.getAnsStatus().equalsIgnoreCase("d")) {
            if (bq.getTrueOption().equalsIgnoreCase("d")) {
                rbD.setBackground(getResources().getDrawable(R.drawable.border_green));
                rbD.setButtonTintList(ContextCompat.getColorStateList(getActivity(), R.color.green));
            } else {
                rbD.setBackground(getResources().getDrawable(R.drawable.border_red));
                rbD.setButtonTintList(ContextCompat.getColorStateList(getActivity(), R.color.red));
            }
        }

        if (PracticeActivity.arrayque.get(count).getIsFavourite() == 1) {
            imgRev.setImageResource(R.mipmap.ic_fill_mark_rev);
            imgRev.setTag("Dark=" + arrayque.get(count).getQuestionID());

        } else {
            imgRev.setImageResource(R.mipmap.ic_mark_rev);
            imgRev.setTag("Light=" + arrayque.get(count).getQuestionID());
        }

        imgRev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String tagName = imgRev.getTag().toString();
                String str[] = tagName.split("=");
                tagName = str[0];
                String strID = str[1];

                if (tagName.equalsIgnoreCase("Light")) {
                    imgRev.setImageResource(R.mipmap.ic_fill_mark_rev);
                    imgRev.setTag("Dark=" + strID);
                    dbp.updateRev(1, bq.getQuestionID());
                    bq.setIsFavourite(1);
                } else {
                    imgRev.setImageResource(R.mipmap.ic_mark_rev);
                    imgRev.setTag("Light=" + strID);
                    dbp.updateRev(0, bq.getQuestionID());
                    bq.setIsFavourite(0);
                }
            }
        });


        rbA.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (rbA.isChecked()) {
                    strChecked = "A";
                    click(strChecked);
                    bq.setAnsStatus("A");
                }
            }
        });
        rbB.setOnClickListener(new View.OnClickListener() {
            @TargetApi(Build.VERSION_CODES.LOLLIPOP)
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onClick(View v) {
                if (rbB.isChecked()) {
                    strChecked = "B";
                    click(strChecked);
                    bq.setAnsStatus("B");
                }
            }
        });
        rbC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (rbC.isChecked()) {
                    strChecked = "C";
                    click(strChecked);
                    bq.setAnsStatus("C");
                }
            }
        });
        rbD.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (rbD.isChecked()) {
                    strChecked = "D";
                    click(strChecked);
                    bq.setAnsStatus("D");
                }
            }
        });

        return view;
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    void click(String ans) {
        rbA.setBackground(getResources().getDrawable(R.drawable.boarder));
        rbB.setBackground(getResources().getDrawable(R.drawable.boarder));
        rbC.setBackground(getResources().getDrawable(R.drawable.boarder));
        rbD.setBackground(getResources().getDrawable(R.drawable.boarder));
        rbA.setButtonTintList(ContextCompat.getColorStateList(getActivity(), R.color.colorPrimary));
        rbB.setButtonTintList(ContextCompat.getColorStateList(getActivity(), R.color.colorPrimary));
        rbC.setButtonTintList(ContextCompat.getColorStateList(getActivity(), R.color.colorPrimary));
        rbD.setButtonTintList(ContextCompat.getColorStateList(getActivity(), R.color.colorPrimary));

        if (ans.equalsIgnoreCase(PracticeActivity.arrayque.get(count).getTrueOption())) {
            if (ans.equalsIgnoreCase("A")) {
                rbA.setBackground(getResources().getDrawable(R.drawable.border_green));
                rbA.setButtonTintList(ContextCompat.getColorStateList(getActivity(), R.color.green));
                rbA.setChecked(true);
                rbB.setEnabled(false);
                rbC.setEnabled(false);
                rbD.setEnabled(false);
            } else if (ans.equalsIgnoreCase("B")) {
                rbB.setBackground(getResources().getDrawable(R.drawable.border_green));
                rbB.setButtonTintList(ContextCompat.getColorStateList(getActivity(), R.color.green));
                rbB.setChecked(true);
                rbC.setEnabled(false);
                rbA.setEnabled(false);
                rbD.setEnabled(false);
            } else if (ans.equalsIgnoreCase("C")) {
                rbC.setBackground(getResources().getDrawable(R.drawable.border_green));
                rbC.setButtonTintList(ContextCompat.getColorStateList(getActivity(), R.color.green));
                rbC.setChecked(true);
                rbA.setEnabled(false);
                rbB.setEnabled(false);
                rbD.setEnabled(false);
            } else {
                rbD.setBackground(getResources().getDrawable(R.drawable.border_green));
                rbD.setButtonTintList(ContextCompat.getColorStateList(getActivity(), R.color.green));
                rbD.setChecked(true);
                rbC.setEnabled(false);
                rbB.setEnabled(false);
                rbA.setEnabled(false);
            }
        } else {
            if (ans.equalsIgnoreCase("A")) {
                rbA.setBackground(getResources().getDrawable(R.drawable.border_red));
                rbA.setButtonTintList(ContextCompat.getColorStateList(getActivity(), R.color.red));
            } else if (ans.equalsIgnoreCase("B")) {
                rbB.setBackground(getResources().getDrawable(R.drawable.border_red));
                rbB.setButtonTintList(ContextCompat.getColorStateList(getActivity(), R.color.red));
            } else if (ans.equalsIgnoreCase("C")) {
                rbC.setBackground(getResources().getDrawable(R.drawable.border_red));
                rbC.setButtonTintList(ContextCompat.getColorStateList(getActivity(), R.color.red));
            } else {
                rbD.setBackground(getResources().getDrawable(R.drawable.border_red));
                rbD.setButtonTintList(ContextCompat.getColorStateList(getActivity(), R.color.red));
            }
        }
    }

    public void trueOption() {
        Bundle args = getArguments();
        count = args.getInt("page_position");
        if (PracticeActivity.arrayque.get(count).getTrueOption() != null) {
            click(PracticeActivity.arrayque.get(count).getTrueOption());
            bq.setAnsStatus(PracticeActivity.arrayque.get(count).getTrueOption());
        }
    }

}