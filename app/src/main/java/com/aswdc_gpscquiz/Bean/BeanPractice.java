package com.aswdc_gpscquiz.Bean;

public class BeanPractice {

    private int questionID;
    private int categoryID;
    private int languageID;
    private String question;
    private String optionA;
    private String optionB;
    private String optionC;
    private String optionD;
    private String answer;
    private String trueOption;
    private int testid;
    private int IsFavourite;
    private int FQuestionID;
    private int testwiseQueID;
    private int testID;
    private String ansStatus;
    private String testStatus;
    private int isCorrect;
    private String isAttempt;

    public int getCategoryID() {
        return categoryID;
    }

    public void setCategoryID(int categoryID) {
        this.categoryID = categoryID;
    }

    public int getTestwiseQueID() {
        return testwiseQueID;
    }

    public void setTestwiseQueID(int testwiseQueID) {
        this.testwiseQueID = testwiseQueID;
    }

    public int getTestID() {
        return testID;
    }

    public void setTestID(int testID) {
        this.testID = testID;
    }

    public String getAnsStatus() {
        if(ansStatus==null)
            ansStatus="";
        return ansStatus;
    }

    public void setAnsStatus(String ansStatus) {
        this.ansStatus = ansStatus;
    }

    public String getTestStatus() {
        return testStatus;
    }

    public void setTestStatus(String testStatus) {
        this.testStatus = testStatus;
    }

    public int getIsCorrect() {
        return isCorrect;
    }

    public void setIsCorrect(int isCorrect) {
        this.isCorrect = isCorrect;
    }

    public String getIsAttempt() {
        return isAttempt;
    }

    public void setIsAttempt(String isAttempt) {
        this.isAttempt = isAttempt;
    }

    public int getQuestionID() {
        return questionID;
    }

    public void setQuestionID(int questionID) {
        this.questionID = questionID;
    }

    public int getLanguageID() {
        return languageID;
    }

    public void setLanguageID(int languageID) {
        this.languageID = languageID;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getOptionA() {
        return optionA;
    }

    public void setOptionA(String optionA) {
        this.optionA = optionA;
    }

    public String getOptionB() {
        return optionB;
    }

    public void setOptionB(String optionB) {
        this.optionB = optionB;
    }

    public String getOptionC() {
        return optionC;
    }

    public void setOptionC(String optionC) {
        this.optionC = optionC;
    }

    public String getOptionD() {
        return optionD;
    }

    public void setOptionD(String optionD) {
        this.optionD = optionD;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public String getTrueOption() {
        return trueOption;
    }

    public void setTrueOption(String trueOption) {
        this.trueOption = trueOption;
    }

    public int getTestid() {
        return testid;
    }

    public void setTestid(int testid) {
        this.testid = testid;
    }

    public int getIsFavourite() {
        return IsFavourite;
    }

    public void setIsFavourite(int isFavourite) {
        IsFavourite = isFavourite;
    }

    public int getFQuestionID() {
        return FQuestionID;
    }

    public void setFQuestionID(int FQuestionID) {
        this.FQuestionID = FQuestionID;
    }
}
