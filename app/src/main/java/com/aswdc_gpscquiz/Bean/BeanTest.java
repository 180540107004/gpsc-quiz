package com.aswdc_gpscquiz.Bean;

public class BeanTest {

    private int testid;
    private int langID;
    private String testName;
    private int totalQuestion;
    private int totalCorrect;
    private int totalAttempt;
    private String testCreateDate;

    public String getTestSubmitDate() {
        return testSubmitDate;
    }

    public void setTestSubmitDate(String testSubmitDate) {
        this.testSubmitDate = testSubmitDate;
    }

    private String testSubmitDate;

    public int getTestid() {
        return testid;
    }

    public int getLangID() {
        return langID;
    }

    public void setLangID(int langID) {
        this.langID = langID;
    }

    public void setTestid(int testid) {
        this.testid = testid;
    }

    public String getTestName() {
        return testName;
    }

    public void setTestName(String testName) {
        this.testName = testName;
    }

    public int getTotalQuestion() {
        return totalQuestion;
    }

    public void setTotalQuestion(int totalQuestion) {
        this.totalQuestion = totalQuestion;
    }

    public int getTotalCorrect() {
        return totalCorrect;
    }

    public void setTotalCorrect(int totalCorrect) {
        this.totalCorrect = totalCorrect;
    }

    public int getTotalAttempt() {
        return totalAttempt;
    }

    public void setTotalAttempt(int totalIncorrect) {
        this.totalAttempt = totalIncorrect;
    }

    public String getTestCreateDate() {
        return testCreateDate;
    }

    public void setTestCreateDate(String testCreateDate) {
        this.testCreateDate = testCreateDate;


    }
}
