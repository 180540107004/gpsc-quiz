package com.aswdc_gpscquiz.Bean;

import java.util.List;

public class BeanTestList {

    private List<BeanTest> results;

    public List<BeanTest> getResults() {
        return results;
    }

    public void setTest(List<BeanTest> results) {
        this.results = results;
    }
}
