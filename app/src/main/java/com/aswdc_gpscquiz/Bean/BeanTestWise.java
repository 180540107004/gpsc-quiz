package com.aswdc_gpscquiz.Bean;

public class BeanTestWise {

    private int testwiseQueID;
    private int testID;
    private int questionID;
    private String ansStatus;
    private String testStatus;
    private String isCorrect;
    private String isAttempt;

    public String isCorrect() {
        return isCorrect;
    }

    public void setCorrect(String correct) {
        isCorrect = correct;
    }

    public String isAttempt() {
        return isAttempt;
    }

    public void setAttempt(String attempt) {
        isAttempt = attempt;
    }

    public int getTestwiseQueID() {
        return testwiseQueID;
    }

    public void setTestwiseQueID(int testwiseQueID) {
        this.testwiseQueID = testwiseQueID;
    }

    public int getTestID() {
        return testID;
    }

    public void setTestID(int testID) {
        this.testID = testID;
    }

    public int getQuestionID() {
        return questionID;
    }

    public void setQuestionID(int questionID) {
        this.questionID = questionID;
    }

    public String getAnsStatus() {
        return ansStatus;
    }

    public void setAnsStatus(String ansStatus) {
        this.ansStatus = ansStatus;
    }

    public String getTestStatus() {
        return testStatus;
    }

    public void setTestStatus(String testStatus) {
        this.testStatus = testStatus;
    }
}
