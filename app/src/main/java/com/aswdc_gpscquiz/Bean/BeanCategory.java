package com.aswdc_gpscquiz.Bean;

public class BeanCategory {

    private int categoryID;
    private String categoryName;
    private String categoryNameGUJ;

    public int getCategoryID() {
        return categoryID;
    }

    public void setCategoryID(int categoryID) {
        this.categoryID = categoryID;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getCategoryNameGUJ() {
        return categoryNameGUJ;
    }

    public void setCategoryNameGUJ(String categoryNameGUJ) {
        this.categoryNameGUJ = categoryNameGUJ;
    }
}
